package cs.niu.edu.z1803493;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;



public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	//User Recommendation 
    	//pearson
    	DataModel model = new FileDataModel(new File("data/ratings.csv"));
		UserSimilarity p_similarity = new PearsonCorrelationSimilarity(model);
		UserNeighborhood p_neighborhood = new ThresholdUserNeighborhood(0.1, p_similarity, model);
		UserBasedRecommender p_recommender = new GenericUserBasedRecommender(model, p_neighborhood, p_similarity);
		List<RecommendedItem> p_recommendations = p_recommender.recommend(100, 10);
		System.out.println("Pearson Correlation Similarity : ");
		for (RecommendedItem p_recommendation : p_recommendations) {
		  System.out.println(p_recommendation);
		}
		//cosine similarity
		UserSimilarity c_similarity = new UncenteredCosineSimilarity(model);
		UserNeighborhood c_neighborhood = new ThresholdUserNeighborhood(0.1, c_similarity, model);
		UserBasedRecommender c_recommender = new GenericUserBasedRecommender(model, c_neighborhood, c_similarity);
		List<RecommendedItem> c_recommendations = c_recommender.recommend(100, 10);
		System.out.println("Cosine Similarity : ");
		for (RecommendedItem c_recommendation : c_recommendations) {
		  System.out.println(c_recommendation);
    }
		//Evaluate  Recommendation 
		   System.out.println("Pearson Similarity Precision and Recall");
		   for(int i=2;i<=50;){
		   RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
		   RecBuilder_local Rbuilder = new RecBuilder_local();
		   Rbuilder.i = i;
		   Rbuilder.property=RecBuilder_local.similarity.PearsonSimilarity;
		    IRStatistics stats = evaluator.evaluate(Rbuilder, null, model, null, 10,
		    		GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 0.01);
		    System.out.println(i+","+stats.getPrecision()+","+stats.getRecall());
		    i++;
		}
		   System.out.println("Cosine Similarity Precision and Recall");
		   for(int i=2;i<=50;){
			   RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
			   RecBuilder_local Rbuilder = new RecBuilder_local();
			   Rbuilder.i = i;
			   Rbuilder.property=RecBuilder_local.similarity.CosineSimilarity;
			    IRStatistics stats = evaluator.evaluate(Rbuilder, null, model, null, 10,
			    		GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 0.1);
			    System.out.println(i+","+stats.getPrecision()+","+stats.getRecall());
			    i++;
			}

	

	    
//Item Recommendation 
	    
	    try {
			DataModel dm = new FileDataModel(new File("data/ratings.csv"));
			ItemSimilarity sim = new LogLikelihoodSimilarity(dm);
			GenericItemBasedRecommender recommender = new GenericItemBasedRecommender(dm,sim);
			
			int x=1;
			for(LongPrimitiveIterator items = dm.getItemIDs();items.hasNext();){
				Long ItemId = items.nextLong();
				List<RecommendedItem> recommendations = recommender.mostSimilarItems(ItemId, 10);
				if(x==1 || x==51){
				for(RecommendedItem recommendation : recommendations)
					System.out.println(ItemId+","+ recommendation.getItemID()+","+recommendation.getValue());
				}
				x=x+1;
			}
		} catch (IOException e) {
			System.out.println("There is some error");
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.println("There is Numeric error");
			e.printStackTrace();
		} catch (TasteException e) {
			System.out.println("Taste Exception");
			e.printStackTrace();
		}		
 }
}


class RecBuilder_local implements RecommenderBuilder {
	int i;
	public enum similarity{
		PearsonSimilarity,CosineSimilarity
	} 
	similarity property;
    public Recommender buildRecommender(DataModel model) throws TasteException {
    	UserSimilarity sim =null;
    	if (property == similarity.PearsonSimilarity)
    		sim= new PearsonCorrelationSimilarity(model);
    	else if (property == similarity.CosineSimilarity)
    		 sim = new UncenteredCosineSimilarity(model);
		UserNeighborhood neighborhood = new NearestNUserNeighborhood(i, sim, model);
        return new GenericUserBasedRecommender(model, neighborhood, sim);
    }}