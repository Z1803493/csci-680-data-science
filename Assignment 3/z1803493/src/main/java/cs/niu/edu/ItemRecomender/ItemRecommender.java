package cs.niu.edu.ItemRecomender;

import java.util.List;
import java.io.File;
import java.io.IOException;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;


public class ItemRecommender {

	public static void main(String[] args) {
		
		try {
			DataModel dm = new FileDataModel(new File("data/ratings.csv"));
			ItemSimilarity sim = new LogLikelihoodSimilarity(dm);
			GenericItemBasedRecommender recommender = new GenericItemBasedRecommender(dm,sim);
			
			int x=1;
			for(LongPrimitiveIterator items = dm.getItemIDs();items.hasNext();){
				Long ItemId = items.nextLong();
				List<RecommendedItem> recommendations = recommender.mostSimilarItems(ItemId, 10);
				if(x==1 || x==148){
				for(RecommendedItem recommendation : recommendations)
					System.out.println(ItemId+","+ recommendation.getItemID()+","+recommendation.getValue());
				}
				x=x+1;
			}
		} catch (IOException e) {
			System.out.println("There is some error");
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.println("There is Numeric error");
			e.printStackTrace();
		} catch (TasteException e) {
			System.out.println("Taste Exception");
			e.printStackTrace();
		}		
	}

}
