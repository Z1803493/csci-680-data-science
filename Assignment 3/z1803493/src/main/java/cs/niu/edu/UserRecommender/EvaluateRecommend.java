package cs.niu.edu.UserRecommender;

import java.io.File;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

public class EvaluateRecommend {

	public static void main(String[] args) throws Exception {
	   DataModel model = new FileDataModel(new File("data/ratings.csv"));
	   System.out.println("Pearson Similarity Precision and Recall");
	   for(int i=2;i<=50;){
	   RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
	   RecBuilder_local Rbuilder = new RecBuilder_local();
	   Rbuilder.i = i;
	   Rbuilder.property=RecBuilder_local.similarity.PearsonSimilarity;
	    IRStatistics stats = evaluator.evaluate(Rbuilder, null, model, null, 10,
	    		GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 0.01);
	    System.out.println(i+","+stats.getPrecision()+","+stats.getRecall());
	    i++;
	}
	   System.out.println("Cosine Similarity Precision and Recall");
	   for(int i=2;i<=50;){
		   RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
		   RecBuilder_local Rbuilder = new RecBuilder_local();
		   Rbuilder.i = i;
		   Rbuilder.property=RecBuilder_local.similarity.CosineSimilarity;
		    IRStatistics stats = evaluator.evaluate(Rbuilder, null, model, null, 10,
		    		GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 0.1);
		    System.out.println(i+","+stats.getPrecision()+","+stats.getRecall());
		    i++;
		}

}
}

class RecBuilder_local implements RecommenderBuilder {
	int i;
	public enum similarity{
		PearsonSimilarity,CosineSimilarity
	} 
	similarity property;
    public Recommender buildRecommender(DataModel model) throws TasteException {
    	UserSimilarity sim =null;
    	if (property == similarity.PearsonSimilarity)
    		sim= new PearsonCorrelationSimilarity(model);
    	else if (property == similarity.CosineSimilarity)
    		 sim = new UncenteredCosineSimilarity(model);
		UserNeighborhood neighborhood = new NearestNUserNeighborhood(i, sim, model);
        return new GenericUserBasedRecommender(model, neighborhood, sim);
    }
}
