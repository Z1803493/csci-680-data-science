package cs.niu.edu.UserRecommender;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

public class UserRecommender {

	public static void main(String[] args) throws Exception {
		DataModel model = new FileDataModel(new File("data/ratings.csv"));
		
		UserSimilarity p_similarity = new PearsonCorrelationSimilarity(model);
		UserNeighborhood p_neighborhood = new ThresholdUserNeighborhood(0.1, p_similarity, model);
		UserBasedRecommender p_recommender = new GenericUserBasedRecommender(model, p_neighborhood, p_similarity);
		List<RecommendedItem> p_recommendations = p_recommender.recommend(100, 10);
		System.out.println("Pearson Correlation Similarity : ");
		for (RecommendedItem p_recommendation : p_recommendations) {
		  System.out.println(p_recommendation.getItemID());
		}
		
		UserSimilarity c_similarity = new UncenteredCosineSimilarity(model);
		UserNeighborhood c_neighborhood = new ThresholdUserNeighborhood(0.1, c_similarity, model);
		UserBasedRecommender c_recommender = new GenericUserBasedRecommender(model, c_neighborhood, c_similarity);
		List<RecommendedItem> c_recommendations = c_recommender.recommend(100, 10);
		System.out.println("Cosine Similarity : ");
		for (RecommendedItem c_recommendation : c_recommendations) {
		  System.out.println(c_recommendation.getItemID());
		}

	}

}
